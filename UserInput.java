package bonus;
import java.util.Scanner; 
public class UserInput {
	static  Scanner scan = new Scanner(System.in);
    public static int readNumber(String mgs) {
    	System.out.println(mgs);
    	return scan.nextInt();
    }
    public static String readName(String mgs) {
    	System.out.println(mgs);
    	return scan.next();
    }
    
    public static double readDouble(String mgs) {
    	System.out.println(mgs);
    	return scan.nextDouble();
    }

}